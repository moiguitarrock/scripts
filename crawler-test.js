var Crawler = require("js-crawler");

const domain = process.argv[2];

if (!domain) {
  console.log('Please define a valid domain')
  process.exit(0);
}

new Crawler().configure({ depth: 0 }).crawl(domain, function onSuccess(page) {
  if (page.url === domain) {
    console.log(
      page.content.includes('class="day clickable"')
        ? "Available days"
        : "Not available days"
    );
    process.exit(0);
  }
});
